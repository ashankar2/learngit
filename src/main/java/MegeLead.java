import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class MegeLead {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		
		Set<String> windref = driver.getWindowHandles();
		List <String> listref = new ArrayList<String>();
		listref.addAll(windref);
		driver.switchTo().window(listref.get(1));
		driver.findElementByXPath("(//input[@type='text'])[1]").sendKeys("10706");
		driver.findElementByXPath("//td[@class='x-panel-btn-td']").click();
		Thread.sleep(5000);
		driver.findElementByXPath("//a[@class='linktext'][1]").click();
		
		driver.switchTo().window(listref.get(0));
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		
		Set<String> windref1 = driver.getWindowHandles();
		List <String> listref1 = new ArrayList<String>();
		listref1.addAll(windref1);
		driver.switchTo().window(listref1.get(1));
		driver.findElementByXPath("(//input[@type='text'])[1]").sendKeys("10707");
		driver.findElementByXPath("//button[@class = 'x-btn-text'][1]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//a[@class='linktext'][1]").click();
		
		
		
		driver.switchTo().window(listref1.get(0));
		driver.findElementByLinkText("Merge").click();
		
		driver.switchTo().alert().accept();
		
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys("10706");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[7]").click();
		Thread.sleep(2000);
		
		String text = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		
		if(text.contains("No records to display")) 
		{
		System.out.println(text);
		}
		else System.out.println("Not Match :(");
		
		driver.close();

	}

}
