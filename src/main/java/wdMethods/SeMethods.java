package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import util.BasicReport;

public class SeMethods extends BasicReport implements WdMethods{
	public RemoteWebDriver driver;	
	public int i = 1;

	public void startApp(String browser, String url) // to launch browser, load url, maximize browser window, wait 30 seconds
	{
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodrivers.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("The Browser "+browser+" Launched Successfully","pass");
		} catch (WebDriverException e) {
			reportStep("The Browser "+browser+" not Launched","fail");
		}
		takeSnap();
	}





	public WebElement locateElement(String locator, String locValue) // ==> to locate a web element 
	{
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "classname": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "LinkText": return driver.findElementByLinkText(locValue);
		case "name":return driver.findElementByName(locValue);
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) // To use a particular locator - asbove method defines all. So no need of this method
	{ 

		return null;
	}

	@Override
	public void type(WebElement ele, String data)  // Enter a particular text in a field
	{	try {
		ele.sendKeys(data);
		//System.out.println("The Data "+data+" Entered Successfully");
		reportStep("The Data "+data+" Entered Successfully", "pass");
	} catch (WebDriverException e) {
		reportStep("The Data "+data+" not Entered", "fail");

	}
	takeSnap();
	}

	@Override
	public void click(WebElement ele) // To click a particular web element
	{
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
	}

	@Override
	public void clickWithoutSnap(WebElement ele) // To click a particular web element
	{
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
	}

	@Override
	public String getText(WebElement ele) { //Nat 1 ==> To get the Text of a particular web element

		String text = ele.getText();
		System.out.println("The text is: "+text);
		takeSnap();
		return null;
	}


	public void selectDropDownUsingText(WebElement ele, String value) //Nat 2 ==> Drop Down select an Option - Using Visible Text
	{
		Select sc = new Select(ele);
		sc.selectByVisibleText(value);
		takeSnap();


	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) //Nat 3 ==> Drop Down select an option - Using Index
	{

		Select sc = new Select(ele);
		sc.selectByIndex(index);
		takeSnap();

	}

	@Override
	public boolean verifyTitle(String expectedTitle) // Nat 4 ==> Title Check - Partially Matching
	{
		String title = driver.getTitle();
		if(title.contains(expectedTitle))
		{
			System.out.println("Title "+title+" Matched");
		}
		else 
		{
			System.out.println("Title Not Matched");
		}
		return false;
	}

	@Override
	public boolean verifyExactTitle(String expectedTitle) // Nat 8 ==> Title Check - Partially Matching
	{
		String title = driver.getTitle();
		if(title.equals(expectedTitle))
		{
			System.out.println("Exact Title "+title+" Matched");
		}
		else 
		{
			System.out.println("Exact Title Not Matched");
		}
		return false;
	}


	@Override
	public void verifyExactText(WebElement ele, String expectedText) // Nat 5 ==> Text Check - Exactly Matching 
	{
		String ExactText = ele.getText();
		if (ExactText.equals(expectedText))
		{
			System.out.println("Exact Text is "+ExactText);
		} 
		else 
		{
			System.out.println("Text Mismatch");
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) // Nat 9 ==> Text Check - Partially Matching
	{
		String Text = ele.getText();
		if (Text.contains(expectedText))
		{
			System.out.println("Text is "+Text);
		} 
		else 
		{
			System.out.println("Text Mismatch");
		}


	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) //Nat 10 ==> Attribute Value Check - Exactly Matching
	{

		String ExactAttribute = ele.getAttribute(attribute);

		if(ExactAttribute.equalsIgnoreCase(value))
		{
			System.out.println("The Value of "+attribute+" is matching and is "+ExactAttribute);
		}
		else 
		{
			System.out.println("Attribute Value Mismatch");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) // Nat 11 ==> Attribute VAlue Check - Partially Matching
	{
		String AttributeText = ele.getAttribute(attribute);

		if(AttributeText.contains(value))
		{
			System.out.println("The Value of "+attribute+" is matching and is "+AttributeText);
		}
		else 
		{
			System.out.println("Attribute Value Mismatch");
		}


	}

	@Override
	public void verifySelected(WebElement ele) // Nat 18 ==> verify a particular web element is selected. try - selected. catch - not selected
	{
		try 
		{
			if(ele.isSelected())
			{
				System.out.println("The element "+ele+" is Selected");
			}
			else
				System.out.println("Not Selected");
		} 
		catch (Exception e) 
		{
			throw new NoSuchElementException();
			//e.printStackTrace();
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) // Nat 17 ==> verify a particular web element is displayed. try - displayed. catch - not displayed
	{
		try 
		{
			if(ele.isDisplayed())
				System.out.println("The Element "+ele+" is Displayed");
			else
				System.out.println("Not Displayed");
			takeSnap();
		} 

		catch (Exception e) 
		{
			throw new NoSuchElementException();
			//e.printStackTrace();
		}

	}

	@Override
	public void switchToWindow(int index) // Nat 12 ==> To Switch from Current Window to required window

	{
		try 
		{
			Set<String> Windows = driver.getWindowHandles();
			List<String> WindowList = new ArrayList<String>();
			WindowList.addAll(Windows);
			driver.switchTo().window(WindowList.get(index));
			takeSnap();
		} 
		catch (Exception e) 
		{
			throw new NoSuchWindowException("Window Not Found");
		}

	}

	@Override
	public void switchToFrame(WebElement ele)  // Nat 13 ==> To switch to a particular frame

	{
		try 
		{
			driver.switchTo().frame(ele);
			takeSnap();
		} 
		catch (Exception e) 
		{
			throw new NoSuchFrameException("Frame Not Found");			
		}
	}

	@Override
	public void acceptAlert() // Nat 14 ==> To Accept the Alert
	{
		driver.switchTo().alert().accept();
		takeSnap();

	}

	@Override
	public void dismissAlert() // Nat 15 ==> To Dismiss the Alert
	{
		driver.switchTo().alert().dismiss();
		takeSnap();
	}

	@Override
	public String getAlertText() // Nat 16 ==> To get the Alert Text as a string
	{
		String AlertText = driver.switchTo().alert().getText();
		System.out.println("The Text Present in the Alert is "+AlertText);
		takeSnap();
		return null;
	}


	public void takeSnap() // To take the snapshot of each steps executed
	{ 
		try 
		{
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, desc);
		} 

		catch (IOException e) 
		{
			e.printStackTrace();
		}
		i++;
	} 

	@Override
	public void closeBrowser() // Nat 7 - Close the Current Active Browser
	{
		driver.close(); 
	}

	@Override
	public void closeAllBrowsers() // Nat 6 - All Browser Windows will be closed
	{
		driver.quit(); 
	}

}
