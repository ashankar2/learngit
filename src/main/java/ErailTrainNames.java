import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErailTrainNames {

	public static void main(String[] args) throws InterruptedException {


		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();

		driver.get("https://erail.in");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("TVC",Keys.TAB);

		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> row = table.findElements(By.tagName("tr")); // Here .findElements() ==> will return a list of Web Elements. These need to be stored a List
		int rowcount = row.size();
		
		/*WebElement checkbox = driver.findElementByXPath("//input[@id='chkSelectDateOnly']");
		
		if(checkbox.isSelected()) 
		{
			checkbox.click();
		}


		Thread.sleep(2000);
*/		
		for (int i = 0; i <rowcount; i++) 
		{

			WebElement value = row.get(i);
			if (value.findElements(By.tagName("td")).size()>1) 
			{
				List<WebElement> rowlist = value.findElements(By.tagName("td"));
				String seccolumn = rowlist.get(1).getText();
				System.out.println(seccolumn);	
			}


		}

	}

}
