
import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {

	static ExtentHtmlReporter html;
	static ExtentReports extent;
	public ExtentTest test;

	@BeforeSuite

	public void startResult() {

		html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true); //maintain previous reports
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
	public void startTest() 
	{
		test = extent.createTest("Login", "Login into Leaftaps"); //to create each test case
	}
	
	public void runReport() throws IOException 
	{

		//if()
		test.pass("username is entered",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.fail("password is not entered",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.assignAuthor("kannan");
		test.assignCategory("Functional");
	}

	@AfterSuite
	public void endResult() {


		extent.flush(); //write report

	}


}







