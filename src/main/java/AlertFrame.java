import org.openqa.selenium.chrome.ChromeDriver;

public class AlertFrame {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		
		driver.switchTo().frame(1); //Switch the Console from main html page to Frame
		driver.findElementByXPath("//button[text() = 'Try it']").click();
		driver.switchTo().alert().sendKeys("Nataraj S");
		driver.switchTo().alert().accept();
		
		
		
		

	}

}
