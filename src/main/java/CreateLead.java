import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

public static void main(String[] args) 
{

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe"); //to invoke Chrome Browser
		
		ChromeDriver driver=new ChromeDriver(); //ChromeDriver class will launch the Chrome Browser
		
		driver.manage().window().maximize(); //to maximize the browser window

		driver.get("http://leaftaps.com/opentaps"); //to pass the url
		
		
		driver.findElementById("username").sendKeys("DemoSalesManager"); //to pass value in Username field-->Inspected the element using finElementById() method
		driver.findElementById("password").sendKeys("crmsfa"); // .sendKeys()--> to pass the value in the field
		driver.findElementByClassName("decorativeSubmit").click(); // .click()--> to click the web element
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("IBMBuddies");
		driver.findElementById("createLeadForm_firstName").sendKeys("Nataraj");
		driver.findElementById("createLeadForm_lastName").sendKeys("S");

		WebElement source = driver.findElementById("createLeadForm_dataSourceId"); //Here we need to select a Drop Down field. The Drop Down field will have multiple options to select. In order to select a particular option from the drop down we need to store this drop down in an object of Web Element Data type
		Select vt= new Select(source); //"Select" is the class where all the Drop Down related actions are defined. We have to create an object of the "Select" class for the particular Drop Down and perform the actions
		vt.selectByVisibleText("Public Relations");

		WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
		Select value = new Select(marketing);
		value.selectByValue("CATRQ_CARNDRIVER");

		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select id = new Select(industry);
		List<WebElement> indlist = id.getOptions();
		id.selectByIndex(indlist.size()-3);

		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
		Select pc = new Select(currency);
		List<WebElement> currlst = pc.getOptions();
		pc.selectByIndex(currlst.size()-5);

		WebElement owner = driver.findElementById("createLeadForm_ownershipEnumId");
		Select own = new Select(owner);
		List<WebElement> o = own.getOptions();
		own.selectByIndex(o.size()-2);

		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select sc = new Select(country);
		List<WebElement> s = sc.getOptions();
		sc.selectByIndex(s.size()-3);

		//driver.findElementByClassName("smallSubmit").click();



		//driver.close();
	}

}
