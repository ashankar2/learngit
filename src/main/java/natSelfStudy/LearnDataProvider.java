package natSelfStudy;

import static org.testng.Assert.assertTrue;

//import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LearnDataProvider 
{
	WebDriver driver;

	@Test(dataProvider="Credentials")
	public void OpenFacebook(String EmaiID, String Password)
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://www.facebook.com/");

		driver.findElement(ById.id("email")).sendKeys(EmaiID);

		driver.findElement(ById.id("pass")).sendKeys(Password);

		driver.findElement(ByXPath.xpath("//input[@value='Log In']")).click();

		Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Facebook"),"User is not able to login - Invalid Credentials"); 
		//Assert will execute first command and proceed further if the condition is true. If condition is false it execute the second command

		System.out.println("Page Title verified - User is able to login successfully");

	}
	
	
	@AfterMethod // to close the browser after test execution
	public void closeSession()
	{
		driver.close();
	}

	
	@DataProvider(name = "Credentials")
	public Object [][] InputData()
	{
		Object [][] data = new Object[3][2];

		data[0][0] = "natarajsankaran36.s@gmail.com";
		data[0][1] = "TPGsnr07";

		data[1][0] = "natarajsankaran36.s@gmail.com";
		data[1][1] = "WrongPassword1";

		data[2][0] = "wrongEmailID@gmail.com";
		data[2][1] = "TPGsnr07";

		return data;
	}

}
