import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) throws InterruptedException 
	{

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();//maximixe the window

		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");

		driver.findElementById("userRegistrationForm:userName").sendKeys("NatarajS7");
		driver.findElementById("userRegistrationForm:password").sendKeys("IBM123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("IBM123");
		
		WebElement SecurityQuestion = driver.findElementById("userRegistrationForm:securityQ");
		Select SQ = new Select(SecurityQuestion);
		SQ.selectByVisibleText("What was the name of your first school?");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Model School");
		
		WebElement PreferredLanguage = driver.findElementById("userRegistrationForm:prelan");
		Select PL = new Select(PreferredLanguage);
		PL.selectByValue("hi");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("NATARAJ");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("VIJAYA");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("SANKARAN");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		WebElement Date = driver.findElementById("userRegistrationForm:dobDay");
		Select D = new Select(Date);
		D.selectByVisibleText("07");
		
		WebElement Month = driver.findElementById("userRegistrationForm:dobMonth");
		Select M = new Select(Month);
		M.selectByValue("05");
		
		WebElement Year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select Y = new Select(Year);
		Y.selectByIndex(7);
		
		WebElement Occupation = driver.findElementById("userRegistrationForm:occupation");
		Select O = new Select(Occupation);
		List<WebElement> Occupationoptions = O.getOptions();
		O.selectByIndex(Occupationoptions.size()-5);
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("100020003000");
		driver.findElementById("userRegistrationForm:idno").sendKeys("AYEPN3409J");
		
		WebElement Country = driver.findElementById("userRegistrationForm:countries");
		Select Cou = new Select(Country);
		Cou.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("natarajsankaran36.s@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("8547062938");
		
		WebElement Nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select Nat = new Select(Nationality);
		Nat.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("1/9 3rd Main Road");
		driver.findElementById("userRegistrationForm:street").sendKeys("Venakateswara Nagar");
		driver.findElementById("userRegistrationForm:area").sendKeys("Porur");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600089",Keys.TAB); //to enter TAB key
		
		Thread.sleep(3000); //for 3 seconds break 
		
		WebElement City = driver.findElementById("userRegistrationForm:cityName");
		Select Cit = new Select(City);
		List<WebElement> Cityoptions = Cit.getOptions();
		Cit.selectByIndex(Cityoptions.size()-1);
		
		Thread.sleep(3000);
		
		WebElement Postoffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select PO = new Select(Postoffice);
		List<WebElement> Post = PO.getOptions();
		PO.selectByIndex(Post.size()-1);
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("04443366");
		driver.findElementById("userRegistrationForm:resAndOff:0").click();
		
		
		
		
		



	}

}
