package util;

import java.io.IOException;

import javax.management.RuntimeErrorException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {

	static ExtentHtmlReporter html;
	static ExtentReports extent;
	public ExtentTest test;
	public String testCaseName,testDesc,author,category, excelfilename;

	@BeforeSuite(groups = "common")

	public void startResult() { //html file creation and creating template class - only once at beginning (suite level)

		html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true); //maintain previous reports
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
	
	@BeforeClass(groups = "common")
	public void startTest() //for each test case this method will run
	{
		test = extent.createTest(testCaseName, testDesc); //to create each test case
		test.assignAuthor(author);
		test.assignCategory(category);
	}

	public void reportStep(String desc, String Status) //step level description replace the sysout statement inside SeMethods
	{


		if(Status.equalsIgnoreCase("pass")) 
		{
			test.pass(desc);
		}
		else if(Status.equalsIgnoreCase("fail")) 
		{
			test.fail(desc);
			throw new RuntimeException();
		}
	}

	@AfterSuite(groups = "common")
	public void endResult() {


		extent.flush(); //write report - after end of all the test cases execution - only once at the end

	}


}







