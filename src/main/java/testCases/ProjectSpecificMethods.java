package testCases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SendKeysAction;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Excel.LearnExcel;
import wdMethods.SeMethods;

public class ProjectSpecificMethods extends SeMethods 

{
	@BeforeMethod

	@Parameters({"browser","url","username","password"})

	public void login(String browserName, String URL, String uname, String pwd) 

	{
		startApp(browserName,URL); // To Launch the Browser

		WebElement eleUsername = locateElement("id","username"); // Locate Username field
		type(eleUsername, uname); //Enter Used Id

		verifyPartialAttribute(eleUsername, "class", "Login"); // To verify the attribute value with the user given value partially

		WebElement elePassword = locateElement("id", "password"); // Locate Password field
		type(elePassword, pwd); // Enter Password

		verifyExactAttribute(elePassword, "type", "Password"); // To verify the attribute value with the user given value exactly

		WebElement eleLogin = locateElement("classname", "decorativeSubmit"); //Locate Login button
		click(eleLogin); // Click the Login Button

		WebElement eleCRMSFA = locateElement("LinkText", "CRM/SFA"); //Locate CRM/SFA link
		click(eleCRMSFA); //click on the CRM/SFA link
		

	}

	@AfterMethod
	
	public void close() 
	
	{
		closeBrowser();
	}
	

	@DataProvider(name = "fetchdata")
	public Object[][] getData() throws IOException 
	{
		return LearnExcel.setData(excelfilename);
	}

		
}
