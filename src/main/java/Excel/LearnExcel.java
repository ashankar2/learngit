package Excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel 

{
	public static Object[][] setData(String filename) throws IOException // Integrated: setData() ==> method to get the test data from spreadsheet using the filename of the sheet specified in the given path during test case execution run time

	{
		XSSFWorkbook wbook = new XSSFWorkbook("./Excel/"+filename+".xlsx"); //XSSFWorkbook - Class which contains all the methods to deal with a Excel Spreadsheet

		XSSFSheet CreateLeadSheet = wbook.getSheetAt(0); //.getSheet() ==> method to get the spreadsheet from the specified path. It will return XSSFSheet data type object

		int rowCount = CreateLeadSheet.getLastRowNum(); //.getLastRowNum() ==> to get the row count of the sheet
		System.out.println("The Row Count is "+rowCount);

		int columnCount = CreateLeadSheet.getRow(0).getLastCellNum(); //.getRow(0).getLastCellNum() ==> to get the column count of the sheet through first row
		System.out.println("The Column Count is "+columnCount);

		Object [][] data = new Object [rowCount][columnCount]; // Integrated: Declared a 2 dimensional array of object data type 

		for (int i = 1; i <= rowCount; i++) 
		{
			XSSFRow rowValue = CreateLeadSheet.getRow(i); // .getRow() ==> navigates to a particular row. It returns a XSSFRow data type object

			for (int j = 0; j < columnCount; j++) 
			{
				XSSFCell cell = rowValue.getCell(j); // .getCell() ==>  navigates to the cell representing the column. It returns a XSSFCell data type object

				String stringCellValue = cell.getStringCellValue(); // .getStringCellValue() ==> returns the value of the cell as a String

				data[i-1][j] = stringCellValue; // Integrated 

				//System.out.println(cell);
			} 
		}
		return data;


	}

}
